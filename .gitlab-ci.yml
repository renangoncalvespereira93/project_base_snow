image: cirrusci/flutter:beta

stages:
  - lint
  - publish

flutter_analyze:
  stage: lint
  script:
    - cd example || exit 1
    - flutter pub get
    - cd .. || exit 1
    - flutter analyze --pub
    - flutter format -l 80 -n . --set-exit-if-changed
  only:
    refs:
      - merge_requests
    changes:
      - lib/**/*.dart
      - test/**/*.dart
      - example/lib/**/*.dart
      - example/test/**/*.dart

dry-run:
  stage: publish
  script:
    - flutter pub get
    - flutter pub publish --dry-run
  only:
    - merge_requests

tag:
  image: docker:stable
  services:
    - docker:stable-dind
  stage: publish
  script:
    - |
      if [ -z "${GITLAB_API_TOKEN}" ]; then
        echo "Missing GITLAB_API_TOKEN environment variable"
        exit 1
      fi

      export TAG_NAME="$(awk '/^version: /{print $NF}' pubspec.yaml)"
      docker run --rm curlimages/curl --fail --request POST --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" \
      --data-urlencode "tag_name=v${TAG_NAME}" \
      --data-urlencode "ref=master" \
      --data-urlencode "release_description=Check the [CHANGELOG.md](/CHANGELOG.md)" \
      "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/repository/tags"
  environment:
    name: pub-dev
    url: https://pub.dev/packages/twilio_programmable_video
  when: manual
  only:
    - master

pub-dev:
  stage: publish
  script:
    - |
      if [ -z "${PUB_DEV_PUBLISH_ACCESS_TOKEN}" ]; then
        echo "Missing PUB_DEV_PUBLISH_ACCESS_TOKEN environment variable"
        exit 1
      fi

      if [ -z "${PUB_DEV_PUBLISH_REFRESH_TOKEN}" ]; then
        echo "Missing PUB_DEV_PUBLISH_REFRESH_TOKEN environment variable"
        exit 1
      fi

      if [ -z "${PUB_DEV_PUBLISH_TOKEN_ENDPOINT}" ]; then
        echo "Missing PUB_DEV_PUBLISH_TOKEN_ENDPOINT environment variable"
        exit 1
      fi

      if [ -z "${PUB_DEV_PUBLISH_EXPIRATION}" ]; then
        echo "Missing PUB_DEV_PUBLISH_EXPIRATION environment variable"
        exit 1
      fi

      cat <<EOF > ~/.pub-cache/credentials.json
      {
        "accessToken":$(echo "${PUB_DEV_PUBLISH_ACCESS_TOKEN},
        "refreshToken":$(echo "${PUB_DEV_PUBLISH_REFRESH_TOKEN},
        "tokenEndpoint":"${PUB_DEV_PUBLISH_TOKEN_ENDPOINT}",
        "scopes":["https://www.googleapis.com/auth/userinfo.email","openid"],
        "expiration":${PUB_DEV_PUBLISH_EXPIRATION}
      }
      EOF
    - flutter pub get
    - flutter pub publish -f
  only:
    - tags