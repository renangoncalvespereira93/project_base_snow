import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:reorderables/reorderables.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
import 'package:flutter_snow_base/src/widgets/error_snow/error_snow_widget.dart';

class ListViewResourceWidget<T> extends StatelessWidget {
  final bool useSliver;
  final Resource<List<T>> resource;
  final Widget loadingTile;
  final Widget Function(T data) tileMapper;
  final Widget Function(int index, Widget tile)? separatorBuilder;
  final Widget? emptyWidget;
  final Future<void> Function()? refresh;
  final EdgeInsets? padding;
  final List<Widget> topWidgets;
  final Widget Function(AppException e)? errorWidget;
  final Axis scrollDirection;
  final bool reverse;
  final ScrollController? controller;
  final bool? primary;
  final ScrollPhysics? physics;
  final bool shrinkWrap;
  final bool addAutomaticKeepAlives;
  final bool addRepaintBoundaries;
  final bool addSemanticIndexes;
  final double? cacheExtent;
  final int? semanticChildCount;
  final DragStartBehavior dragStartBehavior;
  final ScrollViewKeyboardDismissBehavior keyboardDismissBehavior;
  final String? restorationId;
  final Clip clipBehavior;
  final double? itemExtent;
  final bool reorderableList;
  final ReorderCallback? onReorder;
  final int loadingTileQuantity;

  const ListViewResourceWidget({
    Key? key,
    this.useSliver = false,
    required this.resource,
    required this.loadingTile,
    required this.tileMapper,
    this.refresh,
    this.emptyWidget,
    this.padding,
    this.topWidgets = const [],
    this.errorWidget,
    this.scrollDirection = Axis.vertical,
    this.reverse = false,
    this.controller,
    this.primary,
    this.physics,
    this.shrinkWrap = true,
    this.itemExtent,
    this.addAutomaticKeepAlives = true,
    this.addRepaintBoundaries = true,
    this.addSemanticIndexes = true,
    this.cacheExtent,
    this.semanticChildCount,
    this.dragStartBehavior = DragStartBehavior.start,
    this.keyboardDismissBehavior = ScrollViewKeyboardDismissBehavior.manual,
    this.restorationId,
    this.clipBehavior = Clip.hardEdge,
    this.loadingTileQuantity = 2,
    this.separatorBuilder,
  })  : reorderableList = false,
        onReorder = null,
        super(key: key);

  const ListViewResourceWidget.reorderable({
    Key? key,
    required this.resource,
    required this.loadingTile,
    required this.tileMapper,
    required this.onReorder,
    this.refresh,
    this.emptyWidget,
    this.topWidgets = const [],
    this.errorWidget,
    this.padding,
    this.reverse = false,
    this.controller,
    this.shrinkWrap = true,
    this.loadingTileQuantity = 2,
    this.separatorBuilder,
  })  : scrollDirection = Axis.vertical,
        useSliver = false,
        primary = null,
        physics = null,
        itemExtent = null,
        cacheExtent = null,
        semanticChildCount = null,
        dragStartBehavior = DragStartBehavior.start,
        keyboardDismissBehavior = ScrollViewKeyboardDismissBehavior.manual,
        restorationId = null,
        clipBehavior = Clip.hardEdge,
        addAutomaticKeepAlives = true,
        addRepaintBoundaries = true,
        addSemanticIndexes = true,
        reorderableList = onReorder != null,
        super(key: key);

  const ListViewResourceWidget.sliver({
    Key? key,
    this.useSliver = true,
    required this.resource,
    required this.loadingTile,
    required this.tileMapper,
    this.refresh,
    this.separatorBuilder,
    this.emptyWidget,
    this.topWidgets = const [],
    this.errorWidget,
    this.addAutomaticKeepAlives = true,
    this.addRepaintBoundaries = true,
    this.addSemanticIndexes = true,
    this.loadingTileQuantity = 2,
  })  : padding = null,
        scrollDirection = Axis.vertical,
        reverse = false,
        controller = null,
        primary = null,
        physics = null,
        shrinkWrap = false,
        itemExtent = null,
        cacheExtent = null,
        semanticChildCount = null,
        dragStartBehavior = DragStartBehavior.start,
        keyboardDismissBehavior = ScrollViewKeyboardDismissBehavior.manual,
        restorationId = null,
        clipBehavior = Clip.hardEdge,
        reorderableList = false,
        onReorder = null,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    var child = useSliver
        ? SliverList(
            delegate: SliverChildListDelegate.fixed(
              _generateList(
                resource,
              ),
              addAutomaticKeepAlives: addAutomaticKeepAlives,
              addRepaintBoundaries: addRepaintBoundaries,
              addSemanticIndexes: addSemanticIndexes,
            ),
          )
        : reorderableList
            ? ReorderableColumn(
                onReorder: onReorder ?? (a, b) {},
                mainAxisSize: shrinkWrap ? MainAxisSize.min : MainAxisSize.max,
                padding: padding,
                verticalDirection:
                    reverse ? VerticalDirection.up : VerticalDirection.down,
                scrollController: controller ?? ScrollController(),
                header: topWidgets.length == 1
                    ? topWidgets.first
                    : Column(
                        mainAxisSize: MainAxisSize.min,
                        children: topWidgets,
                      ),
                children: _generateList(resource),
              )
            : ListView(
                padding: padding,
                scrollDirection: scrollDirection,
                reverse: reverse,
                controller: controller,
                primary: primary,
                physics: physics,
                shrinkWrap: shrinkWrap,
                itemExtent: itemExtent,
                addAutomaticKeepAlives: addAutomaticKeepAlives,
                addRepaintBoundaries: addRepaintBoundaries,
                addSemanticIndexes: addSemanticIndexes,
                cacheExtent: cacheExtent,
                semanticChildCount: semanticChildCount,
                dragStartBehavior: dragStartBehavior,
                keyboardDismissBehavior: keyboardDismissBehavior,
                restorationId: restorationId,
                clipBehavior: clipBehavior,
                children: _generateList(
                  resource,
                ),
              );
    if (refresh != null && !useSliver) {
      return RefreshIndicator(
        onRefresh: refresh!,
        child: child,
      );
    }
    return child;
  }

  List<Widget> _generateList(
    Resource<List<T>> resource,
  ) {
    var listWidgets = <Widget>[];
    if (resource.data != null) {
      listWidgets = (resource.data ?? []).map<Widget>(tileMapper).toList();
      if (separatorBuilder != null) {
        listWidgets = listWidgets
            .asMap()
            .map((index, child) {
              return MapEntry(index, separatorBuilder!(index, child));
            })
            .values
            .toList();
      }
    }
    switch (resource.status) {
      case Status.loading:
        for (var i = 0; i < loadingTileQuantity; i++) {
          listWidgets.add(loadingTile);
        }
        break;
      case Status.success:
        if (emptyWidget != null &&
            (resource.data == null || (resource.data ?? []).isEmpty)) {
          listWidgets.add(emptyWidget ?? const SizedBox.shrink());
        }
        break;
      case Status.failed:
        listWidgets.add(
          errorWidget != null
              ? errorWidget!(resource.error ?? const AppException())
              : ErrorSnowWidget(
                  resource.message,
                  onTryAgain: refresh,
                  key: UniqueKey(),
                ),
        );
        break;
      default:
        break;
    }
    if (!reorderableList) {
      listWidgets.insertAll(0, topWidgets);
    }
    return listWidgets;
  }
}
