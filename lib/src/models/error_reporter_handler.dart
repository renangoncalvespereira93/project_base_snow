// ignore: import_of_legacy_library_into_null_safe
import 'package:catcher/model/localization_options.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:catcher/model/platform_type.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:catcher/model/report.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:catcher/model/report_handler.dart';
import 'package:flutter/foundation.dart';

class ErrorReporterHandler implements ReportHandler {
  final Future<void> Function(FlutterErrorDetails errorDetails) errorReporter;
  ErrorReporterHandler(
    this.errorReporter, {
    LocalizationOptions? localizationOption,
  }) : _localizationOptions = localizationOption;

  @override
  Future<bool> handle(Report error) async {
    try {
      await errorReporter(error.errorDetails);
      return true;
      // ignore: empty_catches
    } catch (e) {}
    return false;
  }

  @override
  List<PlatformType> getSupportedPlatforms() {
    return [
      PlatformType.android,
      PlatformType.iOS,
      PlatformType.web,
      PlatformType.unknown,
    ];
  }

  ///Location settings
  LocalizationOptions? _localizationOptions;

  @override
  LocalizationOptions? get localizationOptions => _localizationOptions;

  @override
  void setLocalizationOptions(LocalizationOptions localizationOptions) {
    _localizationOptions = localizationOptions;
  }
}
