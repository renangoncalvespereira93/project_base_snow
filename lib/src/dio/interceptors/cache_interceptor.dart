// ignore: import_of_legacy_library_into_null_safe
import 'package:dio/dio.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:dio_http_cache/dio_http_cache.dart';

class CacheInterceptor extends InterceptorsWrapper {
  final InterceptorsWrapper cacheInterceptor =
      DioCacheManager(CacheConfig()).interceptor;

  @override
  Future onRequest(RequestOptions options) async {
    /// use [ Options(extra: {"useCache": false}) ] to control the cache, true by default
    if (options.extra['useCache'] ??
        true && options.responseType != ResponseType.stream) {
      final _optionsCache = buildCacheOptions(
        Duration(days: 3),
        maxStale: Duration(days: 7),
      );
      options.extra.addAll(_optionsCache.extra);
    }
    final _optionsOrResponse = await cacheInterceptor.onRequest(options);

    if (_optionsOrResponse is Response) {
      return _optionsOrResponse;
    }

    return super.onRequest(_optionsOrResponse);
  }

  @override
  Future onResponse(Response response) async {
    response = await cacheInterceptor.onResponse(response);
    return super.onResponse(response);
  }

  @override
  Future onError(DioError dioError) async {
    final _dioErrorOrResponse = await cacheInterceptor.onError(dioError);
    if (_dioErrorOrResponse is Response) {
      return _dioErrorOrResponse;
    }
    return super.onError(_dioErrorOrResponse);
  }
}
