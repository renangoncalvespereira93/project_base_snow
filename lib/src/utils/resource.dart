import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter_snow_base/src/models/app_exception.dart';
import 'package:flutter_snow_base/src/models/status.dart';

import '../../flutter_snow_base.dart';

@immutable
class Resource<T> {
  final Status status;
  final T? data;
  final String message;
  final AppException? error;
  // ignore: prefer_final_fields
  // ignore: prefer_function_declarations_over_variables
  static AppException Function(dynamic e) _errorMapper =
      (e) => AppException(exception: e, message: e.toString());

  const Resource(
      {this.data, required this.status, this.message = '', this.error});

  // ignore: use_setters_to_change_properties
  static void setErrorMapper(AppException Function(dynamic e) errorMapper) {
    _errorMapper = errorMapper;
  }

  Resource<O> transformData<O>(
    O Function(T? data) transformData,
  ) =>
      Resource<O>(
        data: transformData(data),
        status: status,
        error: error,
        message: message,
      );

  Resource<T?> mergeStatus(Resource? other) {
    if (other == null) {
      return this;
    }
    if (status == other.status) {
      return this;
    } else if (status == Status.failed) {
      return this;
    } else if (other.status == Status.failed) {
      return other.transformData<T?>((data) => this.data);
    } else if (status == Status.loading) {
      return this;
    } else {
      return other.transformData<T?>((data) => this.data);
    }
  }

  static Resource<T> loading<T>({T? data}) =>
      Resource<T>(data: data, status: Status.loading);

  static Resource<T> failed<T>({dynamic error, T? data}) {
    final _error = _errorMapper(error);
    return Resource<T>(
      error: _error,
      data: data,
      status: Status.failed,
      message: _error.message,
    );
  }

  static Resource<T> success<T>({T? data}) =>
      Resource<T>(data: data, status: Status.success);

  static Future<Resource<T>> asFuture<T>(Future<T> Function() req) async {
    try {
      final res = await req();
      return success<T>(data: res);
      // ignore: avoid_catches_without_on_clauses
    } catch (e) {
      final _errorMapped = _errorMapper(e);
      debugPrint(e.toString());
      return failed(
        error: _errorMapped,
        data: _errorMapped.data is T ? _errorMapped.data : null,
      );
      // ignore: avoid_catches_without_on_clauses
    }
  }

  static Resource<T> asRequest<T>(T Function() req) {
    try {
      final res = req();
      return success<T>(data: res);
      // ignore: avoid_catches_without_on_clauses
    } catch (e) {
      final _errorMapped = _errorMapper(e);
      debugPrint(e.toString());
      return failed(
        error: _errorMapped,
        data: _errorMapped.data is T ? _errorMapped.data : null,
      );
      // ignore: avoid_catches_without_on_clauses
    }
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Resource<T> &&
        o.status == status &&
        o.data == data &&
        o.message == message &&
        o.error == error;
  }

  @override
  int get hashCode {
    return status.hashCode ^ data.hashCode ^ message.hashCode ^ error.hashCode;
  }
}
