extension StringExtension on String {
  String getNameFromEnum() {
    if (!contains('.')) return this;
    return replaceRange(0, indexOf('.') + 1, '');
  }
}
