import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_snow_base/src/models/status.dart';
import 'resource.dart';

class NetworkBoundResources<ResultType, RequestType> {
  late StreamController<Resource<ResultType>> _result;
  var _resultIsClosed = false;

  Future<Resource<ResultType>> asFuture({
    Future<RequestType> Function()? loadFromDb,
    bool Function(RequestType? data)? shouldFetch,
    required Future<RequestType> Function() createCall,
    ResultType Function(RequestType? result)? processResponse,
    Future Function(RequestType item)? saveCallResult,
  }) {
    assert(
      RequestType == ResultType ||
          (!(RequestType == ResultType) && processResponse != null),
      'You need to specify the `processResponse` when the types are different',
    );
    processResponse ??= (value) => value as ResultType;
    return Resource.asFuture<ResultType>(() async {
      final value = loadFromDb == null ? null : await loadFromDb();
      final _shouldFetch = shouldFetch == null ? true : shouldFetch(value);
      return processResponse!(
        _shouldFetch
            ? await _fetchFromNetwork(createCall, saveCallResult, value)
            : value,
      );
    });
  }

  Stream<Resource<ResultType>> asSimpleStream({
    required Stream<RequestType> Function() createCall,
    ResultType Function(RequestType? result)? processResponse,
  }) async* {
    assert(
      RequestType == ResultType ||
          (!(RequestType == ResultType) && processResponse != null),
      'You need to specify the `processResponse` when the types are different',
    );
    processResponse ??= (value) => value as ResultType;
    yield Resource.loading();
    yield* createCall().map(
      (event) => Resource.success(data: processResponse!(event)),
    );
  }

  Stream<Resource<ResultType>> asResourceStream({
    required Stream<Resource<RequestType>> Function() createCall,
    ResultType Function(RequestType? result)? processResponse,
  }) async* {
    assert(
      RequestType == ResultType ||
          (!(RequestType == ResultType) && processResponse != null),
      'You need to specify the `processResponse` when the types are different',
    );
    processResponse ??= (value) => value as ResultType;
    yield Resource.loading();
    yield* createCall().map(
      (event) => event.transformData(processResponse!),
    );
  }

  Stream<Resource<ResultType>> asStream({
    Stream<RequestType> Function()? loadFromDbStream,
    Future<RequestType> Function()? loadFromDbFuture,
    bool Function(RequestType data)? shouldFetch,
    required Future<RequestType> Function() createCall,
    ResultType Function(RequestType? result)? processResponse,
    Future Function(RequestType item)? saveCallResult,
  }) {
    assert(
      RequestType == ResultType ||
          (!(RequestType == ResultType) && processResponse != null),
      'You need to specify the `processResponse` when the types are different',
    );
    assert(
      loadFromDbStream == null && loadFromDbFuture != null ||
          loadFromDbStream != null && loadFromDbFuture == null,
      'You need to specify at least and only one of `loadFromDbFuture` or `loadFromDbStream` to load your data',
    );
    processResponse ??= (value) => value as ResultType;

    StreamSubscription? localListener;

    _result = StreamController<Resource<ResultType>>.broadcast(
      onCancel: () {
        if (!_result.hasListener) {
          _result.close();
          _resultIsClosed = true;
          if (localListener != null) localListener.cancel();
        }
      },
    );
    _resultIsClosed = false;

    _sinkAdd(_result.sink, Resource.loading());

    Future<void> _fetchData(Future<RequestType> Function() event,
        Sink<Resource<ResultType>> sink) async {
      late RequestType _event;
      Future<ResultType> proccessEvent() async {
        try {
          _event = await event();
        } catch (e) {
          debugPrint(e.toString());
        }
        return processResponse!(_event);
      }

      if (shouldFetch == null) {
        var fetchNetworkResource = Resource.loading<ResultType>();
        final futuresLocalNetwork = Future.wait([
          Future<void>.microtask(() async {
            final result = await proccessEvent();
            if (fetchNetworkResource.status != Status.success) {
              _sinkAdd(
                sink,
                fetchNetworkResource.transformData((data) => result),
              );
            }
          }),
          Future<void>.microtask(() async {
            try {
              var result = await _fetchFromNetwork(createCall, saveCallResult);
              // print("Fetching success");
              fetchNetworkResource =
                  Resource.success(data: processResponse!(result));
              _sinkAdd(sink, fetchNetworkResource);
              // ignore: avoid_catches_without_on_clauses
            } catch (e) {
              // print("Fetching failed");
              fetchNetworkResource = Resource.failed(data: null, error: e);
              _sinkAdd(sink, fetchNetworkResource);
            }
          }),
        ]);
        await futuresLocalNetwork;
        return;
      } else {
        final eventResult = await proccessEvent();
        if (shouldFetch(_event)) {
          // print("Fetch data and call loading");
          _sinkAdd(sink, Resource.loading(data: eventResult));
          try {
            var result = await _fetchFromNetwork(createCall, saveCallResult);
            // print("Fetching success");
            _sinkAdd(sink, Resource.success(data: processResponse!(result)));
            // ignore: avoid_catches_without_on_clauses
          } catch (e) {
            // print("Fetching failed");
            _sinkAdd(sink, Resource.failed(data: null, error: e));
          }
        } else {
          // print("Fetching data its not necessary");
          _sinkAdd(sink, Resource.success(data: eventResult));
        }
      }
    }

    if (loadFromDbStream != null) {
      final localStream = loadFromDbStream().transform(
        StreamTransformer<RequestType, Resource<ResultType>>.fromHandlers(
          handleData: (event, sink) => _fetchData(() async => event, sink),
        ),
      );

      localListener =
          localStream.listen((value) => _sinkAdd(_result.sink, value));
    } else if (loadFromDbFuture != null) {
      _fetchData(loadFromDbFuture, _result.sink)
          .then((value) => _result.close());
    }

    // print("Call loading...");

    return _result.stream;
  }

  void _sinkAdd<T>(Sink<T> sink, T value) {
    if (!_resultIsClosed) {
      try {
        sink.add(value);
        // ignore: empty_catches
      } catch (e) {}
    }
  }

  Future<RequestType> _fetchFromNetwork(
      Future<RequestType> Function() createCall,
      Future Function(RequestType item)? saveCallResult,
      [RequestType? unconfirmedResult]) async {
    if (saveCallResult != null && unconfirmedResult != null) {
      await saveCallResult(unconfirmedResult);
    }

    return await createCall().then((value) async {
      if (saveCallResult != null && value != unconfirmedResult) {
        await saveCallResult(value);
      }
      return value;
    });
  }
}
