# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.0-nullsafety.0] - 2021-01-29

## Improvements

- Improved reordable list with the package `reordables` (the design is suddenly different between the until version
  that used the native `ReordebleListView`)

## [1.0.0-nullsafety.2] - 2020-12-15

### Breaking changes

- Removed `ListViewWidget`
- Updated all packages to support null safety
- Updated `flutter_modular` to `3.0.0`

### Improvements

- Updated all code to support null safety

### Fixes

- Fixed issue [#18](https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/issues/18)

## [0.2.2] - 2020-12-08

### Fixes

- Fixed issue [#17](https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/issues/17)

## [0.2.1] - 2020-11-27

### Added

- Added `reorderable` option to `ListViewResourceWidget`

## [0.2.0] - 2020-11-18 (Breaking changes)

### Breaking changes

- Changed the name `ListViewWidget` to `ListViewResourceWidget`

### Added

- Added variables of `ListView` in `ListViewResourceWidget`
- Added method `asSimpleStream` in `NetworkBoundResources` that allows use a simple stream and transforms
  only the result.
- Added method `asResourceStream` in `NetworkBoundResources` that allows use a simple stream and transforms
  only the result, with the differential that the stream can send by result a `Resource`, so leaving the responsible
  to stream to control if is on error or not.
- Removed the params `shouldFetch` and `saveCallResult` of required to optional of method `asStream` in `NetworkBoundResources`
- Added automatic stream close when using loadFromDbFuture
- Added `asRequest` method in `Resource`, that map errors on functions that does not returns a `Future`.

### Fixes

- Fixed pedantic issues
- Fixed add sink events on closed sinks

## [0.1.7] - 2020-11-17

### Added

- Added `errorWithDataWidget` and `loadingWithDataWidget` attributes on `ResourceWidget`

## [0.1.6] - 2020-11-16

### Fixes

- Export Widget `ErrorSnowWidget`
- Removed unused padding attribute of `ResourceWidget`
- Fixed `errorWidget` of `ResourceWidget`

## [0.1.5] - 2020-10-27

### Fixes

- Export class `UIHelper`

## [0.1.4] - 2020-10-26

### Added

- `WidgetUtil`
- `ColorUtil`
- `CurrencyPtBrInputFormatter` to pt-br currency mask
- `UIHelper` to horizontal and vertical spacement
- Add error log to `Resource.asFuture` when failed

## [0.1.3] - 2020-09-10

## Added

- Added `ListViewWidget`, that helps you to create a list of widgets with a resource, that have 5 states, error,
error with data, loading, loading with data and success. With `RefreshIndicator` and a default `ErrorWidget`.
- Added `ResourceWidget`, that helps you to create a widget with a resource, that have 3 states, error, loading
and success. With a default `ErrorWidget` with a callback function.

### Fixes

- Updated packages

## [0.1.2] - 2020-08-31

### Added

- Updated version of `flutter_modular`

## [0.1.1] - 2020-07-29

### Added

- Added `transformData` and `mergeStatus` methods in Resource object.
- Improvement when occur an error with `Resource.asFuture()`.
- Bug fixes.

## [0.1.0] - 2020-06-26

### Added

- Initial version of the package.
- Thanks to [Denis Viana](https://gitlab.com/denisviana) and to [Lucas Polazzo](https://gitlab.com/LucazzP) that made
  this first version of the package!

[Unreleased]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/compare/master...v1.1.0-nullsafety.0
[1.1.0-nullsafety.0]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v1.1.0-nullsafety.0
[1.0.0-nullsafety.2]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v1.0.0-nullsafety.2
[0.2.2]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v0.2.2
[0.2.1]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v0.2.1
[0.2.0]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v0.2.0
[0.1.7]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v0.1.7
[0.1.6]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v0.1.6
[0.1.5]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v0.1.5
[0.1.4]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v0.1.4
[0.1.3]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v0.1.3
[0.1.2]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v0.1.2
[0.1.1]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v0.1.1
[0.1.0]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v0.1.0
