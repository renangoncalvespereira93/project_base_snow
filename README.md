# Flutter Snow Base! 🧰

<p>
  <a href="https://pub.dev/packages/flutter_snow_blower" target="_blank">
    <img alt="Pub" src="https://img.shields.io/pub/v/flutter_snow_blower?color=orange" />
  </a>
  <a href="https://opensource.org/licenses/BSD-3-Clause" target="_blank">
    <img alt="License: BSD-3-Clause" src="https://img.shields.io/badge/BSD-3-Clause" />
  </a>
  <a href="https://twitter.com/SnowmanLabs" target="_blank">
    <img alt="Twitter: SnowmanLabs" src="https://img.shields.io/twitter/follow/SnowmanLabs.svg?style=social" />
  </a>
</p>

🧰 A package that provides you the essential tools to use in your project, like:

- Easy config to use dark themes
- Ready for Flavors
- Ready to use [Device Preview package](https://pub.dev/packages/device_preview) that helps you to test your responsiveness
- Pre-configured [Catcher](https://pub.dev/packages/catcher)
- Pre-configured locales
- Ready for [i18n_extension package](https://pub.dev/packages/i18n_extension)
- Ready to use [flutter_modular package](https://pub.dev/packages/flutter_modular)

<br>

## 📄 Table of Contents

- **[Why should I use](#-why-should-i-use)**
- **[Motivations](#-motivations)**
- **[Examples](#-examples)**
- **[Install](#-install)**
- **[Usage](#-usage)**
- **[Useful links](#-useful-links)**
- **[Author](#-author)**
- **[Contributing](#-contributing)**
- **[Show your support](#-show-your-support)**
- **[License](#-license)**

## ❓ Why should I use

Flutter snow base helps you to initialize your project with ready tools to use, so if you want to use something, like test
your layout on different screens, you can only change a properly to `true` and voilà! Or you need to pick all the errors
that occurs on the app, the device info and send to you when occurs the error, theres no need any efforts to this! You
only configure how you want to send, and is working!

## 🎯 Motivations

We realized that almost all projects uses the same things as base, like a crash reporter, a flavor config, locales config...
So, this package is basically an union of all the common packages through the projects.

## 📦 Examples

Examples of to-do apps that used flutter_snow_base as base:

- [Daily Nasa photo](https://github.com/LucazzP/daily_nasa_photo)
- [Todo clean](https://gitlab.com/snowman-labs/flutter-devs/project_sample_base)
- [Todo modular](https://gitlab.com/snowman-labs/flutter-devs/todo-app-example-modular)
- [Example clean](https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean)
- [Example clean complete](https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean_complete)
- [Example MVC modular](https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_mvc_modular)
- [Example MVC modular complete](https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_mvc_modular_complete)

## 🔧 Install

Follow this tutorial: [Installation tab](#-installing-tab-)

## 🎉 Usage

>NOTE: This package was made in conjunction with [Flutter Snow Blower], so if you have some doubts of how to initialize this
package in your project, only seed the documentation of [Flutter Snow Blower] or start a project with it to see how it works!

### main.dart file

```dart
void main() {
  RunAppSnow(
    ModularApp(  // Material app
      module: AppModule(),
    ),
    flavorValues: Constants.flavorDev, // Pass your flavor values
    getItInit: () => Resource.setErrorMapper(ErrorMapper.from), // if you want to execute something after flavors but before the app starts to run, this code will run here.
    flavor: Flavor.dev, // Selected flavor
    enableDevicePreview: false, // Enable if you want to use Device Preview.
    errorReporter: (errorDetails) {}, // Use this function to use another crash reporter, like Crashanalytics of firebase.
  );
}
```

### material_app.dart

This is the minimum configuration to use, you can add another parameters, that is the same of MaterialApp.

```dart
class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SnowMaterialApp(
      theme: AppThemeData.themeDataLight,
      darkTheme: AppThemeData.themeDataDark,
    );
  }
}
```

## Useful links

Shift + Alt + O  ->  Organize imports

https://material.io/design/typography/the-type-system.html#type-scale

https://medium.com/flutterando/qual-a-forma-f%C3%A1cil-de-traduzir-seu-app-flutter-para-outros-idiomas-ab5178cf0336
https://pub.dev/packages/i18n_extension#-readme-tab-
https://medium.com/flutter-community/handling-network-calls-like-a-pro-in-flutter-31bd30c86be1
https://medium.com/flutter-community/page-transitions-using-themedata-in-flutter-c24afadb0b5d
https://medium.com/flutter-community/handling-flutter-errors-with-catcher-efce74397862
https://medium.com/flutter-community/flutter-2019-real-splash-screens-tutorial-16078660c7a1
https://iiro.dev/2018/03/02/separating-build-environments/

*Sempre divida os widgets em arquivos, e não em métodos dentro do widget*<br>
https://iiro.dev/2018/12/11/splitting-widgets-to-methods-performance-antipattern/<br>
https://iiro.dev/2018/06/18/putting-build-methods-on-a-diet/<br>
Exemplo: https://github.com/roughike/inKino/blob/development/mobile/lib/ui/event_details/event_backdrop_photo.dart<br>
WidgetTesting: https://iiro.dev/2018/08/22/writing-widget-tests-for-navigation-events/

## 👤 Author

### Snowman labs

<img alt="snowmanlabs-logo" align="right"
  src="https://www.snowmanlabs.com.br/wp-content/uploads/logo_snowmanlabs_fundo_claro.png" height="100"/>

- Website: [snowmanlabs.com](https://snowmanlabs.com)
- Twitter: [@SnowmanLabs](https://twitter.com/SnowmanLabs)
- Github: [@snowmanlabs](https://github.com/snowmanlabs)
- LinkedIn: [@SnowmanLabs](https://linkedin.com/company/snowman-labs)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />
Feel free to check [issues page](https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/issues).
You can also take a look at the [contributing guide](https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/blob/master/CHANGELOG.md).

## 💚 Show your support

<img align="right" width="20%" src="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/raw/master/images/snow_blower_logo.png">

Give a ⭐️ if this project helped you!

## 📝 License

Copyright © 2020 [Snowman labs](https://github.com/snowmanlabs).<br />
This project is [BSD-3 Clause](https://opensource.org/licenses/BSD-3-Clause) licensed.

[clean]: https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html
[Flutter Snow Blower]: https://pub.dev/packages/flutter_snow_blower